<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\Jawaban;
use App\Models\jawabanPertanyaan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JawabanController extends Controller
{
    /**
     * Menampilkan daftar jawaban.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jawabans = jawabanPertanyaan::all();
        return view('jawabans.index', compact('jawabans'));
    }

    public function store(Request $request)
    {
        DB::table('jawaban_pertanyaan')->insert([
            'isi_jawaban' => $request['isi_jawaban'],
            'users_id' => Auth::id(),
            'pertanyaan_id' => $request['pertanyaan_id'],
            'img' => 'null',
        ]);

        return redirect('/pertanyaan/' . $request['pertanyaan_id']);
    }

    /**
     * Menampilkan detail jawaban.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $jawaban = jawabanPertanyaan::findOrFail($id);
        return view('jawabans.show', compact('jawaban'));
    }

    public function destroy($id)
    {
        DB::table('jawaban_pertanyaan')->where('id', $id)->delete();

        // return redirect('/pertanyaan');
        return redirect()->back();
    }

    // Metode lainnya seperti create, store, edit, update, dan destroy
}

