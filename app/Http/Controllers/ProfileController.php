<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;

class ProfileController extends Controller
{
    /**
     * Menampilkan halaman profil pengguna.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::id();
        $profile = Profile::where('users_id', $id)->first();
        // dd($profile);
        return view('profile.edit', ['profile' => $profile]);
    }

    public function update($id, Request $request)
    {
        // dd($id);
        $request->validate([
            // 'nama' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
        ]);

        $profileById = Profile::find($id);

        // $profileById->nama = $request['nama'];
        $profileById->umur = $request['umur'];
        $profileById->alamat = $request['alamat'];

        $profileById->save();

        return redirect('/profile');
    }
}

