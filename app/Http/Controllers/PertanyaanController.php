<?php

namespace App\Http\Controllers;

use App\Models\jawabanPertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pertanyaan;
use App\Models\Kategori;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;



class PertanyaanController extends Controller
{
    /**
     * Menampilkan daftar pertanyaan.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    public function create()
    {
        $kategori = Kategori::all();
        return view('pertanyaan.create', ['kategori' => $kategori]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'img' => 'required|mimes:jpg,jpeg,png|max:2048',
            // 'users_id' => 'required',
            'kategori_id' => 'required',
        ]);

        $imageName = time() . '.' . $request->img->extension();

        $request->img->move(public_path('img'), $imageName);

        DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'img' => $imageName,
            'users_id' => Auth::id(),
            'kategori_id' => $request['kategori_id'],
        ]);

        // $pertanyaan = new Pertanyaan();

        // $pertanyaan->judul = $request['judul'];
        // $pertanyaan->isi = $request['isi'];
        // $pertanyaan->users_id = $request['users_id'];
        // $pertanyaan->kategori_id = $request['kategori_id'];
        // $pertanyaan->img = $imageName;

        // $pertanyaan->save();
        return redirect('/pertanyaan');
    }

    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $jawaban = jawabanPertanyaan::where('pertanyaan_id', $id)->get();
        return view('pertanyaan.detail', [
            'pertanyaan' => $pertanyaan,
            'jawaban' => $jawaban,
        ]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'img' => 'required|mimes:jpg,jpeg,png|max:2048',
            'users_id' => 'required',
            'kategori_id' => 'required',
        ]);;

        $imageName = time() . '.' . $request->poster->extension();

        $request->poster->move(public_path('poster'), $imageName);

        DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'judul' => $request['judul'],
                'isi' => $request['isi'],
                'img' => $imageName,
                'users_id' => $request['users_id'],
                'kategori_id' => $request['kategori_id'],
            ]);

        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        DB::table('jawaban_pertanyaan')->where('pertanyaan_id', $id)->delete();
        DB::table('pertanyaan')->where('id', $id)->delete();

        return redirect('/pertanyaan');
    }

}

