<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class KategoriController extends Controller
{
    /**
     * Menampilkan daftar kategori.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kategori = Kategori::all();
        return view('kategori.index', ['kategori' => $kategori]);
    }

    public function create()
    {
        return view('kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_kategori' => 'required',
        ]);

        DB::table('kategori')->insert([
            'nama_kategori' => $request['nama_kategori'],
        ]);

        return redirect('/kategori');
    }

    public function show($id)
    {
        $kategori = Kategori::find($id);

        return view('kategori.detail', ['kategori' => $kategori]);
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama_kategori' => 'required',
        ]);;

        DB::table('kategori')
            ->where('id', $id)
            ->update([
                'nama_kategori' => $request['nama_kategori'],
            ]);

        return redirect('/kategori');
    }

    public function destroy($id)
    {
        DB::table('kategori')->where('id', $id)->delete();

        return redirect('/kategori');
    }
}

