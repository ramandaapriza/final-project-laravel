<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;

    protected $table = 'pertanyaan';

    protected $fillable = ['judul', 'isi', 'img', 'users_id', 'kategori_id'];

    public function jawabanpertanyaan()
    {
        return $this->hasManyThrough(User::class, Kategori::class);
    }
}
