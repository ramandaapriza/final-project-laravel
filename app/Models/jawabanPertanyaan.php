<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jawabanPertanyaan extends Model
{
    use HasFactory;

    protected $table = 'jawaban_pertanyaan';

    protected $fillable = ['isi_jawaban', 'img', 'users_id', 'pertanyaan_id'];
}
