<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('/templates/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        @auth
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        @endauth
        @guest
        <a href="#" class="d-block">Belum Login</a>
        @endguest
      </div>
    </div>

    

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
             <li class="nav-item">
                 <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
             <li class="nav-item">
                 <a href="/kategori" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Kategori
                    </p>
                </a>
            </li>
             <li class="nav-item">
                 <a href="/pertanyaan" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Pertanyaan
                    </p>
                </a>
            </li>
            
            @auth
            <li class="nav-item">
                <a href="/profile" class="nav-link">
                   <i class="nav-icon fas fa-user"></i>
                   <p>
                       Profile
                   </p>
               </a>
           </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}" class="nav-link"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                   <i class="bi bi-door-open-fill"></i>
                   <p>
                       Logout
                   </p>
               </a>
           </li>
           <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
            @endauth
            @guest
            <li class="nav-item bg-info">
                <a href="/login" class="nav-link">
                  <p>
                    Login
                  </p>
                </a>
              </li>
            @endguest
            
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>