@extends('layout.master')

@section('title', 'Edit Profile')
@section('content')

    <form action="/profil/{{ $profile['id'] }}" method="POST">
        @csrf

        @method('put')

    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $profile->nama}}">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
      <label >Jenis Kelamin</label>
      <input type="enum" class="form-control"  value="{{ $profile->user->jeniskelamin}}" disabled>
    </div>

    <div class="form-group">
      <label >Umur</label>
      <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{ $profile->umur}}">
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <div class="form-group">
      <label >Alamat</label>
      <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ $profile->alamat}}">
        @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection