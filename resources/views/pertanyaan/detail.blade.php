@extends('layout.master')

@section('title', 'Detail Pertanyaan')

@section('content')

    <div class="row">
        <div class="col">
            <div class="mb-5">
            <div class="">
                <h3 class="text-danger">{{$pertanyaan->judul}}</h3>
                <img src="{{asset('img/'.$pertanyaan->img) }}" style="height: 100px; width: 100px" class="card-img-top mb-3" alt="...">
            </div>
                <h4>Pertanyaan : {{$pertanyaan->isi}}</h4>
        </div>
        </div>
        <div class="col">
            <div class="card text-start">
                <div class="card-body">
                    <h4 class="card-title">
                        <div class="row">
                            <div class="col"><h4>Jawaban</h4></div>
                        </div>
                    </h4>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <form action="{{route('jawaban.store')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="pertanyaan_id" value="{{$pertanyaan->id}}">
                                    <input type="text" class="form-control" name="isi_jawaban"/>
                                    <button class="btn btn-success mt-3" type="submit">Kirim Jawaban</button>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    

    @forelse ($jawaban as $keys => $item)
        <div class="card text-start">
            <div class="card-body">
                <div class="row">
                    <div class="col-11">
                        <div class="d-flex align-items-center">
                            <h5>{{$keys+1}} . {{$item->isi_jawaban}}</h5>
                        </div>
                    </div>
                    <div class="col">
                        @if ($item->users_id == Auth::user()->id)
                            <form action="{{route('jawaban.destroy', $item->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" type="submit">Hapus</button>
                            </form>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div>Jawaban Kosong!</div>
    @endforelse

    
@endsection