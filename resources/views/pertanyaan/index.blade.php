@extends('layout.master')

@section('title', 'Index Pertanyaan')

@section ('content')

<a href="/pertanyaan/create" class="btn btn-primary my-3">Tambah Pertanyaan</a>
<table class="table table-sm">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Pertanyaan</th>
        <th scope="col">Image</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($pertanyaan as $keys=>$pertanyaans)
        <tr>
        <td scope="row">{{ $keys + 1 }}</td>
        <td>{{ $pertanyaans->isi }}</td>
        <td>
          <img src="{{asset('img/'.$pertanyaans->img) }}" style="height: 100px; width: 100px" class="card-img-top mb-3" alt="...">
        </td>
        <td class="d-flex">
          <a href="/pertanyaan/{{ $pertanyaans->id }}" class="btn btn-info btn-sm"><i class="bi bi-book"></i></a>

          @if ($pertanyaans->users_id == Auth::user()->id)
            <a href="" class="btn btn-warning btn-sm"><i class="bi bi-pencil-square"></i></a>
            <form action="/pertanyaan/{{ $pertanyaans->id }}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger btn-sm"><i class="bi bi-trash-fill"></i></button>
            </form>
          @endif

        </td>
        </tr>
    @empty
    <tr>
        <td>Pertanyaan Kosong!</td>
    </tr>
        
    @endforelse
        
    </tbody>
  </table>
@endsection