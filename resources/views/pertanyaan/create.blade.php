@extends('layout.master')

@section('title', 'Tambahkan Pertanyaan')

@section('content')

<form action="/pertanyaan" method="POST" enctype="multipart/form-data">
    @csrf
<div class="form-group">
    <label >Judul Pertanyaan</label>
    <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul">
      @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </div>
  <div class="form-group">
    <label >Isi Pertanyaan</label>
    <input type="text" class="form-control @error('isi') is-invalid @enderror" name="isi">
      @error('isi')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </div>
  
  <div class="form-group">
      <label>Kategori Pertanyaan</label>
      <select name="kategori_id" class="form-control @error('kategori_id') is-invalid @enderror" name="tahun">
          <option value="">--Pilih Kategori--</option>
          @forelse ($kategori as  $item)
                <option value="{{ $item->id }}">{{ $item->nama_kategori }}</option>
            @empty
                <option value="">Tidak Ada Kategori</option>
            @endforelse
      </select>
      @error('kategori_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </div>
  <div class="form-group">
      <label >Image</label>
      <input type="file" class="form-control @error('img') is-invalid @enderror" name="img">
        <!-- @error('img') -->
        <!-- <div class="alert alert-danger">{{ $message }}</div> -->
        <!-- @enderror -->
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  </div>
</form>

@endsection
