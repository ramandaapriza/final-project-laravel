@extends('layout.master')

@section('title', 'Home')

@section ('content')
<ul class="list-group">
    <li class="list-group-item">First item
        <form action="" class="text-right" method="POST">
            @csrf
            @method('delete')
            <a href="" class="btn btn-info btn-sm"><i class="bi bi-book"></i></a>
            <a href="" class="btn btn-warning btn-sm"><i class="bi bi-pencil-square"></i></a>
        <button type="submit" class="btn btn-danger btn-sm"><i class="bi bi-trash-fill"></i></button>
    </li>
    <li class="list-group-item">Second item</li>
    <li class="list-group-item">Third item</li>
  </ul>
@endsection