@extends('layout.master')

@section('title', 'Tambah Kategori')
@section('content')

    <form action="/kategori" method="POST">
        @csrf
    <div class="form-group">
      <label >Nama Kategori</label>
      <input type="text" class="form-control @error('nama_kategori') is-invalid @enderror" name="nama_kategori">
        @error('nama_kategori')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
  
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection