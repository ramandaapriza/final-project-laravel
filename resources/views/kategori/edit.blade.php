@extends('layout.master')

@section('title', 'Edit Kategori')
@section('content')

    <form action="/kategori/{{ $kategori->id }}" method="POST">
        @csrf

        @method('put')

        <div class="form-group">
          <label >Nama Kategori</label>
          <input type="text" class="form-control @error('nama_kategori') is-invalid @enderror" name="nama_kategori" value="{{ $kategori->nama_kategori }}">
            @error('nama_kategori')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection