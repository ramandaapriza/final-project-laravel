@extends('layout.master')

@section('title', 'Detail Kategori')
@section('content')

    <h1 class="text-primary">{{ $kategori->nama_kategori }}</h1>
    <a href="/kategori" class="btn btn-primary btn-sm my-3">Back</a>

@endsection