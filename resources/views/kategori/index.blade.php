@extends('layout.master')

@section('title', 'Index Kategori')

@section ('content')

<a href="/kategori/create" class="btn btn-primary my-3">Tambah Kategori</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Kategori</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $keys=>$kategoris)
            <tr>
            <th scope="row">{{ $keys + 1 }}</th>
            <td>{{ $kategoris->nama_kategori }}</td>
            <td>
                <form action="/kategori/{{ $kategoris->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/kategori/{{ $kategoris->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/kategori/{{ $kategoris->id }}/edit" class="btn btn-warning btn-sm">Update</a>
                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
            </form>
            </td>
            </tr>
        @empty
        <tr>
            <td>Kategori Kosong!</td>
        </tr>
            
        @endforelse
      
    </tbody>
  </table>
@endsection