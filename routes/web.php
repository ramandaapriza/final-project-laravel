<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\KategoriController;

// Route untuk HomeController
Route::get('/', [HomeController::class, 'index']);

Route::get('/master', function () {
    return view('layout.master');
});
Route::get('/', function () {
    return view('welcome');
});


// Route untuk UserController
Route::resource('users', UserController::class);

// Route untuk ProfileController
Route::get('/profile', [ProfileController::class, 'index']);

Route::put('/profile/{id}', [ProfileController::class, 'update']);

// Route untuk PertanyaanController
Route::resource('pertanyaan', PertanyaanController::class);

// Route untuk JawabanController
Route::resource('jawaban', JawabanController::class);

// Route untuk KategoriController
Route::resource('kategori', KategoriController::class);

Auth::routes();


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
